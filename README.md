### Cokey

```bash
Usage: java -jar cokey.jar
  --domain=[your domain] (optional,default value:cokey.xyz)
  --server.port=[available port] (optional,default value:8096)
Apis:
  `GET /startDnsd`: start DNS SERVER as daemon
  `GET /stopDnsd`: stop DNS SERVER
```



#### 1、准备工作
准备用于公网IP的机器: 假设IP为47.95.23.45
准备两个域名: 假设为 a.com , b.com

#### 2、域名设置
1. 添加b.com域名两条A记录:
ns1.b.com -> 47.95.23.45
ns2.b.com -> 47.95.23.45

2. 设置a.com域名根dns服务器地址为
ns1.b.com
ns2.b.com

#### 3、运行程序
1. 在该机器(IP:47.95.23.45)运行程序
java -jar cokey.jar --domain=a.com --server.port=9989

2. 开启dns服务(GET 请求到 /startDnsd)
浏览器访问 http://47.95.23.45:9989/startDnsd

3. 当有DNS请求时，shell中会以log形式展现。



