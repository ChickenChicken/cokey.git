package com.hakedbyme.cokey.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;


public class CommonHelper {



    public String retJson(String message) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> msgMap = new HashMap<>();
            msgMap.put("msg", message);
            return mapper.writeValueAsString(msgMap);

        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }


    }

}
