package com.hakedbyme.cokey.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.hakedbyme.cokey.helper.CommonHelper;
import com.hakedbyme.cokey.helper.DnsHelper;


@RestController
public class DnsdController {


    @Autowired
    private DnsHelper dnsHelper = new DnsHelper();


    private CommonHelper commonHelper = new CommonHelper();
    private static Boolean daemoned = false;



    @RequestMapping("/startDnsd")
    public String startDnsd() {

        if (daemoned.equals(false)) {
            dnsHelper.daemon("start");
            daemoned = true;
            return commonHelper.retJson("Dns daemon starts successfully !");
        } else {
            return commonHelper.retJson("Dns daemon has already been running !" );
        }

    }


    @RequestMapping("/stopDnsd")
    public String stopDnsd() {
        try {
            if (daemoned.equals(true)) {
                dnsHelper.daemon("stop");
                daemoned = false;
                return commonHelper.retJson("Dns daemon stops successfully !");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
        return commonHelper.retJson("Dns daemon is not running !");
    }


}