package com.hakedbyme.cokey;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;


@SpringBootApplication()
@EnableAsync
public class CokeyApplication {



    public static void main(String[] args) {

        if (args.length > 0 && (args[0].equals("-h") || args[0].equals("--help"))){
//            show help
            System.out.println("Usage: java -jar cokey.jar \n" +
                    "\t --domain=[your domain]\t(optional,default value:cokey.xyz)\n" +
                    "\t --server.port=[available port]\t(optional,default value:8096)");
            System.out.println("Apis:\n" +
                    "\t `GET /startDnsd`: start DNS SERVER as daemon\n" +
                    "\t `GET /stopDnsd`: stop DNS SERVER");
            System.exit(1);
        }
        SpringApplication.run(CokeyApplication.class, args);




    }


}
